MAINTAINER_NS=vojtabiberle
NAME=docker-php
VERSION=8.2
REGISTRY=registry.gitlab.com

files = Dockerfile root/etc/php/7.0/mods-available/essentials.ini root/etc/service/php-fpm/run

build: $(files)
	docker build -t ${REGISTRY}/${MAINTAINER_NS}/${NAME}:${VERSION} .

run:
	docker run --rm -i -t ${REGISTRY}/${MAINTAINER_NS}/${NAME}:${VERSION} /sbin/my_init -- bash -l

tag:
	docker tag ${MAINTAINER_NS}/${NAME}:${VERSION} ${REGISTRY}/${MAINTAINER_NS}/${NAME}:${VERSION}

push:
	docker push ${REGISTRY}/${MAINTAINER_NS}/${NAME}:${VERSION}
