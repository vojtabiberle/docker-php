# Use phusion/baseimage as base image. To make your builds reproducible, make
# sure you lock down to a specific version, not to `latest`!
# See https://github.com/phusion/baseimage-docker/blob/master/Changelog.md for
# a list of version numbers.
FROM phusion/baseimage:jammy-1.0.1
MAINTAINER Vojtěch Biberle <vojtech.biberle@gmail.com>

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    LC_ALL=C.UTF-8 add-apt-repository -y ppa:ondrej/php && \
    apt-get update && \
    apt-get install -y \
        php8.2 \
        php8.2-cgi \
        php8.2-curl \
        php8.2-gd \
        php8.2-gmp \
        php8.2-ldap \
        php8.2-mysql \
        php8.2-odbc \
        php8.2-opcache \
        php8.2-pgsql \
        php8.2-pspell \
        php8.2-readline \
        php8.2-sqlite3 \
        php8.2-tidy \
        php8.2-xml \
        php8.2-bcmath \
        php8.2-bz2 \
        php8.2-enchant \
        php8.2-fpm \
        php8.2-imap \
        php8.2-interbase \
        php8.2-intl \
        php8.2-mbstring \
        php8.2-phpdbg \
        php8.2-soap \
        php8.2-sybase \
        php8.2-xsl \
        php8.2-zip \
        php-memcached \
        php-xdebug \
        php-pear \
        git \
        unzip  && \
    apt-get purge -y php8.1-common && \
    rm -rf /etc/php/php8.1

#RUN update-alternatives --set php /usr/bin/php8.2

#RUN apt-get install -y php8.2-dev libsodium-dev libsodium18 && pecl install libsodium-1.0.6 && apt-get purge -y php8.2-dev && apt-get autoremove -y

ADD root /

ENV WEBAPP_DIR /var/www/website
ENV WEBAPP_USER webapp
ENV WEBAPP_GROUP webapp
ENV WEBAPP_GID 1000
ENV WEBAPP_UID 1000
ENV WEBAPP_PHP_FPM_LISTEN 9000

ENV COMPOSER_HOME /composer
ENV COMPOSER_ALLOW_SUPERUSER=1
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer

WORKDIR $WEBAPP_DIR
VOLUME [$WEBAPP_DIR]
RUN phpenmod essentials
#RUN phpenmod libsodium
RUN groupadd -g $WEBAPP_GID $WEBAPP_GROUP && \
    useradd -d $WEBAPP_DIR -g $WEBAPP_GID -u $WEBAPP_UID $WEBAPP_USER && \
    chown $WEBAPP_USER:$WEBAPP_GROUP $WEBAPP_DIR && \
    chown $WEBAPP_USER:$WEBAPP_GROUP /run/php && \
    chown $WEBAPP_USER:$WEBAPP_GROUP /composer
RUN sed -i "s/^;daemonize = yes/daemonize = no/g" /etc/php/8.2/fpm/php-fpm.conf
RUN sed -i "s/^user = www-data/user = $WEBAPP_USER/g" /etc/php/8.2/fpm/pool.d/www.conf && \
    sed -i "s/^group = www-data/group = $WEBAPP_GROUP/g" /etc/php/8.2/fpm/pool.d/www.conf && \
    sed -i "s/^listen.owner = www-data/listen.owner = $WEBAPP_GROUP/g" /etc/php/8.2/fpm/pool.d/www.conf && \
    sed -i "s/^listen.group = www-data/listen.group = $WEBAPP_GROUP/g" /etc/php/8.2/fpm/pool.d/www.conf && \
    sed -i "s@^listen = /run/php/php8.2-fpm.sock@listen = $WEBAPP_PHP_FPM_LISTEN@g" /etc/php/8.2/fpm/pool.d/www.conf && \
    sed -i "s@^;chdir = /var/www@chdir = $WEBAPP_DIR@g" /etc/php/8.2/fpm/pool.d/www.conf && \
    sed -i "s/^;clear_env = no/clear_env = no/g" /etc/php/8.2/fpm/pool.d/www.conf

EXPOSE 9000

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
